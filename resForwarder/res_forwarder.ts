import axios from 'axios';
import {Request, Response, Next} from 'restify';

function registerMiddleware(options: ForwarderOptions): MiddleWareIface {
    const {monitoringServiceUrl} = options;
    const resultUrlEndpoint = `${monitoringServiceUrl}/api/result`;


    return {
        middleWare: (req: Request, res: Response, next: Next) => {
            const original = res.json.bind(res);

            res.json = (code, body, headers?) => {
                axios.post(resultUrlEndpoint, {
                    code,
                    body,
                    target: req.getUrl()
                }, {
                    headers
                });
                original(code, body, headers)
            };

            next();
        }
    }
}

interface MiddleWareIface {
    middleWare(req: Request, res: Response, next: Next): void;
}

type ForwarderOptions = {
    monitoringServiceUrl: string;
}

export default {
    registerMiddleware
}
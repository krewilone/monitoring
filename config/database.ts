import User from "../app/models/User";
import Endpoint from "../app/models/MonitoringEndpoint";
import Result from "../app/models/MonitoringResult";
import {Sequelize} from "sequelize-typescript";
import * as path from 'path';
import {create, router, defaults} from 'json-server';

const sequelize = {
    prepareDb: () => {
        const sequelize = new Sequelize({
            host: "127.0.0.1",
            port: 3306,
            database: 'network',
            dialect: 'mysql',
            username: 'root',
            password: 'Password1',
            modelPaths: [path.normalize(__dirname + '/../app/models')]
        });

        sequelize.query("SET FOREIGN_KEY_CHECKS = 0", {raw: true})
            .then(() => {
                return User.sync({force: true})
                    .then(() => {
                        // Table created
                        return User.create({
                            name: 'John',
                            email: 'Hancock@post.cz',
                            token: "a"
                        });
                    });
            })
            .then(() => {
                return Endpoint.sync({force: true});
            })
            .then(() => {
                return Result.sync({force: true});
            })
            .then(() => {
                return sequelize.query("SET FOREIGN_KEY_CHECKS = 1", {raw: true})
            })
            .catch(error => {
                console.log(error);
            })
    }
};




export {
    sequelize
};
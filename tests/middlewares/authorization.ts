import {authorization, IRequest} from '../../app/middlewares/authorization';
import * as userService from '../../app/service/user_service';
import * as sinon from 'sinon';
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';

const expect = chai.expect;

chai.use(sinonChai);
chai.should();

describe("auth middleware", () => {
    let request;
    let response;
    let next;
    let stub;

    beforeEach(() => {
        request = {
            headers: {
                authorization: "aaaa"
            }
        };

        response = {
            send: sinon.spy()
        };

        stub = sinon.stub(userService, "getUserByAcessToken");
        next = sinon.spy();
    });

    it("authorization without accessToken", async () => {
        request.headers.authorization = "";
        await authorization(request, response, next);

        expect(response.send.calledWith(401)).to.be.ok;
        expect(next.calledWith(false)).to.be.ok;
    });

    it("authorization with user not found", async () => {
        stub.rejects();
        await authorization(request as IRequest, response, next);

        expect(response.send.calledWith(401)).to.be.ok;
        expect(next.calledWith(false)).to.be.ok;
    });

    it("authorization with user found", async () => {
        const ref = {};
        stub.resolves(ref);
        await authorization(request, response, next);

        expect(response.send.should.have.not.been.called).to.be.ok;
        expect(next.calledWith()).to.be.ok;
    });

    afterEach(() => {
        stub.restore();
    })

});
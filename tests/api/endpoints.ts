import axios, {AxiosResponse} from 'axios';
import * as sinon from 'sinon';
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';

const expect = chai.expect;

chai.use(sinonChai);
chai.should();

describe("endpoints api", () => {
    beforeEach(() => {

    });

    it("calls create endpoint with correct data without auth", async () => {
        try {
            await axios.post("http://localhost:3000/api/endpoint", {
                "url" :"http://ahoj.cz",
                "name" : "test"
            });
        } catch(e) {
            const response: AxiosResponse = e.response;

            expect(response.status).to.equal(401);
            expect(response.statusText).to.equal("Unauthorized");
        }
    });

    it("calls create with incorrect data", async () => {
        try {
            await axios.post("http://localhost:3000/api/endpoint", {
                "url" :"ahoj",
                "name" : "test"
            }, {
                headers: {
                    Authorization: "a"
                }
            });
        } catch(e) {
            const response: AxiosResponse = e.response;

            expect(response.status).to.equal(400);
            expect(response.statusText).to.equal("Bad Request");
            expect(response.data.message).to.equal("Validation isUrl on url failed");
        }
    });

    it("calls create endpoint with correct data and auth token", async () => {
        try {
            const resp = await axios.post("http://localhost:3000/api/endpoint", {
                "url" :"http://ahoj.cz",
                "name" : "test"
            }, {
                headers: {
                    Authorization: "a"
                }
            });

            expect(resp.status).to.equal(200);
            expect(resp.statusText).to.equal("OK");
            expect(resp.data.name).to.equal("test");
            expect(resp.data.url).to.equal("http://ahoj.cz");
        } catch(e) {
            console.log(e);
        }
    });
});
import * as fs from 'fs';
import * as restify from 'restify';
import * as path from 'path';
import {promisify} from 'util';
import {sequelize} from './config/database';

const readDirPromise = promisify(fs.readdir);

// create Restify server with the configured name
const app: restify.Server = restify.createServer({name: "network-logger"});
const pathToRoutes: string = path.join(__dirname, '/app/routes');

// parse the body of the request into req.params
app.use(restify.plugins.bodyParser());
app.use(restify.plugins.queryParser());

readDirPromise(pathToRoutes)
    .then(registerRoutesFromJsFiles)
    .catch((err) => {
        throw new Error
    });

function registerRoutesFromJsFiles(files: string[]) {
    files.filter(filterJs)
        .forEach(registerRoute);
}

function filterJs(file: string) {
    return path.extname(file) === '.js'
}

function registerRoute(file: string) {
    const route = require(path.join(pathToRoutes, file));
    route.default(app);
}

sequelize.prepareDb();
app.listen(3000, () => {
    console.log(`network-logger is running at ${app.url}`);
});

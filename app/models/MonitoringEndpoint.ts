import {
    Table,
    Column,
    Model,
    ForeignKey,
    CreatedAt,
    UpdatedAt,
    IsUrl,
    Length,
    Scopes,
    BelongsTo, HasOne
} from 'sequelize-typescript';
import User from './User';
import MonitoringResult from "./MonitoringResult";

@Table
export default class MonitoringEndpoint extends Model<MonitoringEndpoint> {

    @Length({min: 1, max: 25})
    @Column
    name: string;

    @IsUrl
    @Column
    url: string;

    @CreatedAt
    creationDate: Date;

    @Column
    get monitoredInterval(): number {
        return (Date.now() - this.getDataValue("creationDate"))/1000;
    }

    @UpdatedAt
    updatedOn: Date;

    @ForeignKey(() => User)
    @Column
    ownerId: number;
}
import {Table, Column, Model, CreatedAt, ForeignKey, BelongsTo, Scopes, HasOne} from 'sequelize-typescript';
import MonitoringEndpoint from "./MonitoringEndpoint";

@Table
export default class MonitoringResult extends Model<MonitoringResult> {
    @Column
    responseCode: number;

    @Column
    responsePayload: string;

    @CreatedAt
    checkedAt: Date;

    @ForeignKey(() => MonitoringEndpoint)
    @Column
    monitoringEndpointId: number;

    @BelongsTo(() => MonitoringEndpoint)
    monitoringEndpoint: MonitoringEndpoint;
}
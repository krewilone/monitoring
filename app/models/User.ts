import {Table, Column, Model, DataType, Length, IsEmail} from 'sequelize-typescript';

@Table
export default class User extends Model<User> {
    @Length({min: 1, max: 25})
    @Column
    name: string;

    @IsEmail
    @Column
    email: string;

    @Column(DataType.UUID)
    token: string;
}
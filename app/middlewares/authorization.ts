import {Response, Next, Request} from 'restify';
import {getUserByAcessToken} from '../service/user_service';
import User from "../models/User";

async function authorization(req: IRequest, res: Response, next: Next) {
    const headers = req.headers;

    const accessToken = headers.authorization;

    if (!accessToken) {
        return failResponse(res, next);
    }

    try {
        const user = await getUserByAcessToken(accessToken);

        if (!user) {
            return failResponse(res, next);
        }
        req.user = user;
        return next();
    } catch (e) {
        return failResponse(res, next);
    }
}

function failResponse(res: Response, next: Next) {
    res.send(401);
    next(false);
}

export {authorization};

export interface IRequest extends Request {
    user: User
}
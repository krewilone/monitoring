import {Request, Response, Next} from 'restify';
import {BadRequestError, NotAcceptableError} from 'restify-errors';
import {IRequest} from "../middlewares/authorization";
import {getEndpointForUserByUrl} from "../service/endpoint_service";
import {createResult, getResultsForEndpoint} from "../service/result_service";


async function getResults(req: IRequest, res: Response, next: Next) {
    const {query, user} = req;
    try {
        const results = await getResultsForEndpoint(user, query.url, query.limit || 10);
        res.json(200, results);
    } catch (e) {
        return next(new BadRequestError());
    }

    return next();
}

async function responseSent(req: IRequest, res: Response, next: Next) {
    const {body, user} = req;
    const {responseCode, responsePayload, endpointUrl} = body;
    const endpoint = await getEndpointForUserByUrl(user, endpointUrl);

    if (endpoint) {
        const created = await createResult(responseCode, JSON.stringify(responsePayload), endpoint.id);
        res.json(200, created);
        return next();
    }

    return next(new NotAcceptableError("not handled"));
}


export {responseSent, getResults}
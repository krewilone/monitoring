import {Request, Response, Next} from 'restify';
import {BadRequestError} from 'restify-errors';
import {createEndpoint, deleteEndpoint, updateEndpoint} from '../service/endpoint_service';
import {IRequest} from '../middlewares/authorization';

async function create(req: IRequest, res: Response, next: Next) {
    const user = req.user;
    const body = req.body;
    try {
        const created = await createEndpoint(user, body);
        res.json(200, created);
    } catch (e) {
        const message = e.errors.map((error) => error.message).join(" ");
        return next(new BadRequestError(message));
    }
    return next();
}

async function remove(req: IRequest, res: Response, next: Next) {
    const body = req.body;
    try {
        const removed = await deleteEndpoint(body.id);
        if (removed) {
            res.json(200, removed);
        } else {
            return next(new BadRequestError("No endpoint for ID"));
        }
    } catch (e) {
        return next(new BadRequestError(e));
    }
    return next();
}


async function update(req: Request, res: Response, next: Next) {
    const body = req.body;
    try {
        const updated = await updateEndpoint(body.id, body.data);
        if (updated) {
            res.json(200, updated[0]);
        } else {
            return next(new BadRequestError("No endpoint for ID"));
        }
    } catch (e) {
        return next(new BadRequestError(e));
    }
    return next();
}

export {create, update, remove}
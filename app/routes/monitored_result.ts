import {Server} from 'restify';
import {getResults, responseSent} from '../controllers/result_controller';
import {authorization} from '../middlewares/authorization';

export default (api: Server) => {
    api.get('/api/results', authorization, getResults);
    api.post('/api/results', authorization, responseSent)
};
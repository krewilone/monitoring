import {Server} from 'restify';
import {create, update, remove} from '../controllers/endpoint_controller';
import {authorization} from '../middlewares/authorization';

export default (api: Server) => {
    api.post('/api/endpoint', authorization, create);
    api.put('/api/endpoint', authorization, update);
    api.del('/api/endpoint', authorization, remove);
};
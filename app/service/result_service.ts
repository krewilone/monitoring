import MonitoringResult from "../models/MonitoringResult";
import MonitoringEndpoint from "../models/MonitoringEndpoint";
import * as Bluebird from "bluebird";
import User from "../models/User";


function createResult(responseCode, responsePayload, monitoringEndpointId): Bluebird<MonitoringResult> {
    return MonitoringResult.create({
        responseCode,
        responsePayload,
        monitoringEndpointId
    });
}

function getResultsForEndpoint(user: User, url: string, limit: number) {
    return MonitoringResult.findAll({
        include: [{
            model: MonitoringEndpoint,
            where: {
                url,
                ownerId: user.id
            }
        }],
        limit
    });
}

export {createResult, getResultsForEndpoint};

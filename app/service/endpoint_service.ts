import MonitoringEndpoint from '../models/MonitoringEndpoint';
import User from "../models/User";
import * as Bluebird from "bluebird";

function createEndpoint(user: User, data: EndpointData): Bluebird<MonitoringEndpoint> {
    const createData = {
        ownerId: user.id,
        ...data
    };

    return MonitoringEndpoint.create(createData);
}

function getEndpointsForUser(user: User): Bluebird<MonitoringEndpoint[]> {
    const query = {
        where: {
            ownerId: user.id
        }
    };

    return MonitoringEndpoint.findAll(query);
}

function deleteEndpoint(id: number): Bluebird<number> {
    const query = {
        where: {
            id
        }
    };

    return MonitoringEndpoint.destroy(query);
}

function updateEndpoint(id: number, data: EndpointData) {
    const query = {
        where: {
            id
        }
    };

    return MonitoringEndpoint.update(data, query);
}

function getEndpointForUserByUrl(user: User, url: string): Bluebird<MonitoringEndpoint> {
    const query = {
        where: {
            ownerId: user.id,
            url: url
        }
    };

    return MonitoringEndpoint.findOne(query);
}

type EndpointData = {
    name: string;
    url: string;
}


export {
    createEndpoint,
    getEndpointsForUser,
    deleteEndpoint,
    updateEndpoint,
    getEndpointForUserByUrl
}
import User from "../models/User";
import * as Bluebird from "bluebird";


function getUserByAcessToken(token: string): Bluebird<User> {
    const query = {
        where: {
            token
        }
    };

    return User.findOne(query);
}

export {getUserByAcessToken};
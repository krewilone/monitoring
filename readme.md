# Network-monitoring

Service for monitoring network traffic for registered urls. It is based on forwarding all requests from another services to this one using for example forwarder located in resForwarder.

## Local setup

### Prerequisites

* [mysql](https://dev.mysql.com/downloads/mysql/) - Installed mysql server localy with database called "network"
* [nodeJs](https://nodejs.org/en/download/) - Installed NodeJs


### Installing locally

Inside root project directory run

```
npm install
```

### Run locally

Inside root project directory run for devMode

```
npm run dev
```
or
```
npm start
```

for starting the server

```
npm run tests
```

for tests.


